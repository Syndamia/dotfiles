"""
""" vim-plug plugins
"""

call plug#begin('~/.vim/plugged')

""" Visuals
Plug 'morhetz/gruvbox', {'rtp': 'vim'} " Color theme
Plug 'ryanoasis/vim-devicons'          " Icons on stuff like NERDTree
" Plug 'AndrewRadev/popup_scrollbar.vim' " Scrollbar

""" Quality of life
Plug 'tomtom/tcomment_vim'  " Toggle comments (gc, gcc)
" Plug 'Raimondi/delimitMate' " Autocomplete brackets and quotes
Plug 'preservim/nerdtree'   " Browse directories (:NERDTree)
Plug 'mbbill/undotree'      " Easily interact with undo history
Plug 'godlygeek/tabular'    " Line up text by a given character (:Tabularize /CHAR)
Plug 'tpope/vim-eunuch'     " Easy UNIX shell commands
Plug 'alvan/vim-closetag'   " Automatically add HTML closing tags
Plug 'kien/tabman.vim'      " Show open buffers
Plug 'ervandew/supertab'    " Makes <Tab> be nicer to use with Omnicompletion
Plug 'tpope/vim-surround'   " Add and change surrounding quotes and braces
Plug 'ntpeters/vim-better-whitespace' " Highlight trailing whitespaces

""" Software development
Plug 'dense-analysis/ale'            " Linting with LSP
Plug 'editorconfig/editorconfig-vim' " Support for EditorConfig
Plug 'zivyangll/git-blame.vim'       " Show who last edited a line
Plug 'wakatime/vim-wakatime'         " Time tracking via wakatime.com

""" Language integration
Plug 'dNitro/vim-pug-complete', { 'for': ['jade', 'pug'] } " Pug: Omnicompletion integration
Plug 'digitaltoad/vim-pug', { 'for': ['jade', 'pug'] }     " Pug: Filetype detection, identation, syntax highlighting
Plug 'bfrg/vim-cpp-modern', { 'for': ['c', 'cpp'] }        " C/C++: Better syntax highlighting
Plug 'itchyny/vim-haskell-indent', { 'for': 'haskell' }    " haskell: Adds identation
Plug 'vim-scripts/syntaxhaskell.vim', { 'for': 'haskell' } " haskell: Better syntax highlighting
Plug 'AndrewRadev/id3.vim'                                 " mp3: Proper metadata editing
Plug 'vim-scripts/XML-Folding'                             " xml: Proper syntax folding

Plug 'https://gitlab.com/Syndamia/texty-office.vim'
Plug 'tpope/vim-afterimage'

call plug#end()

"""
""" Package plugins
"""

packadd! matchit
