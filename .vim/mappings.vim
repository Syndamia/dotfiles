"""
""" Single key
"""

nnoremap <F1> <ESC>:mksession! .vim-session<CR>
 noremap <F2>  <ESC>:ALERename<CR>
nnoremap <F5>  :UndotreeToggle<CR>
nnoremap <F8> :<C-u>call gitblame#echo()<CR>

nnoremap <Tab> :NERDTreeToggle<CR>

nnoremap <expr> k (v:count == 0 ? 'gk' : 'k')
nnoremap <expr> j (v:count == 0 ? 'gj' : 'j')
xnoremap <expr> k (v:count == 0 ? 'gk' : 'k')
xnoremap <expr> j (v:count == 0 ? 'gj' : 'j')

nnoremap <silent> \| :tab ter ++close lazygit<CR>

"""
""" Shift + key
"""

let g:tabman_toggle = '<S-Tab>'

"""
""" Control + key
"""

inoremap <C-S> <C-O>:w<CR>
inoremap <C-V> <C-O>"+P<CR>
inoremap <C-A> <Esc>ggVG
inoremap <C-R> <C-O><C-R>
inoremap <C-U> <C-O>u

nnoremap <C-S> :w<CR>
nnoremap <C-A> ggVG

xnoremap <C-V> "+p
xnoremap <C-C> "+y
xnoremap <C-X> "+d

nnoremap <C-L> <C-W>l<C-W>=
nnoremap <C-H> <C-W>h<C-W>=
nnoremap <C-J> <C-W>j<C-W>=
nnoremap <C-K> <C-W>k<C-W>=

tnoremap <C-L> <C-W>l<C-W>=
tnoremap <C-H> <C-W>h<C-W>=
tnoremap <C-J> <C-W>j<C-W>=
tnoremap <C-K> <C-W>k<C-W>=

inoremap <C-h>  <C-w>
cnoremap <C-h>  <C-w>
inoremap <C-BS> <C-w>
cnoremap <C-BS> <C-w>

"""
""" Alt + key
"""

" Move lines up/down with Alt-J/K
execute "set <M-j>=\ej"
nnoremap <M-j> <ESC>:m+1<CR>
execute "set <M-k>=\ek"
nnoremap <M-k> <ESC>:m-2<CR>

xnoremap <A-j> :m '>+1<CR>gv=gv
xnoremap <A-k> :m '<-2<CR>gv=gv

execute "set <M-o>=\eo"
nnoremap <M-o> gT
inoremap <M-o> <C-O>gT
tnoremap <M-o> <C-W>gT

execute "set <M-p>=\ep"
nnoremap <M-p> gt
inoremap <M-p> <C-O>gt
tnoremap <M-p> <C-W>gt

"""
""" Two keys
"""

nnoremap gd <Plug>(ale_go_to_definition)
nnoremap gi <Plug>(ale_go_to_implementation)
nnoremap ff <Plug>(ale_hover)
