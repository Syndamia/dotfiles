" Scrolling (shows history) in terminal (except in lazygit)
" Scroll up to activate it, and press right mouse button to deactivate it
" Slightly modified version of: https://github.com/vim/vim/issues/2490#issuecomment-393973253
tmap <silent> <buffer> <ScrollWheelUp> <c-w>:call EnterNormalMode()<CR>

function! ExitNormalMode()
	unmap <silent> <buffer> <RightMouse>
	call feedkeys("a")
endfunction

function! EnterNormalMode()
	if bufname('%') =~ '!lazygit'
		tunmap <buffer> <ScrollWheelUp>
	elseif &buftype == 'terminal' && mode('') == 't'
		call feedkeys("\<c-w>N")
		call feedkeys("\<c-y>")
		map <silent> <buffer> <RightMouse> :call ExitNormalMode()<CR>
	endif
endfunction
