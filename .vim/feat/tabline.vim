let tabcap = ''
let tabmiddlecap = ''

set tabline=%!TablineGenerator()

" Tab data/helper functions {{{
	function! TabFilename(n)
		let buflist = tabpagebuflist(a:n)
		let winnr   = tabpagewinnr(a:n)
		let _       = (expand('#'..buflist[winnr - 1]..':t') !=# '') ? expand('#'..buflist[winnr - 1]..':t') : '[No Name]'

		" Limit the width of tabs, so they don't go out of the screen
		let tabNameLengthMax = &columns/(((tabpagenr('$') > 0) ? tabpagenr('$') : 0) + 5)

		return WebDevIconsGetFileTypeSymbol(_) .. ' ' .. _[0:tabNameLengthMax]
	endfunction

	function! TabReadonly(n)
		let winnr = tabpagewinnr(a:n)
		return gettabwinvar(a:n, winnr, '&readonly') ? '  ' : ''
	endfunction

	function! TabModified(n)
		let winnr = tabpagewinnr(a:n)
		return gettabwinvar(a:n, winnr, '&modified') ? '  ' : (gettabwinvar(a:n, winnr, '&modifiable') ? '' : '  ')
	endfunction
" }}}

" Colors {{{
	autocmd ColorScheme * call TLCreateHighlightGroups()

	" Colorscheme clears highlights https://vi.stackexchange.com/a/3356
	function! TLCreateHighlightGroups()
		hi TLTab    ctermfg=252 ctermbg=242
		hi TLTabSel ctermfg=252 ctermbg=235
		hi TLRest   ctermfg=248 ctermbg=238

		let s:hi_tltab    = hlget('TLTab')[0]
		let s:hi_tltabsel = hlget('TLTabSel')[0]
		let s:hi_tlrest   = hlget('TLRest')[0]
		call hlset([#{name: 'TLTabSelLC',  ctermfg: s:hi_tltab['ctermbg'],    ctermbg: s:hi_tltabsel['ctermbg'] },
		\           #{name: 'TLTabSelRC',  ctermfg: s:hi_tltabsel['ctermbg'], ctermbg: s:hi_tltab['ctermbg'] },
		\           #{name: 'TLTabSelRCF', ctermfg: s:hi_tltabsel['ctermbg'], ctermbg: s:hi_tlrest['ctermbg'] },
		\           #{name: 'TLTabRCF',    ctermfg: s:hi_tltab['ctermbg'],    ctermbg: s:hi_tlrest['ctermbg'] }])
	endfunction
	call TLCreateHighlightGroups()
" }}}

function! TablineGenerator()
	let s = ''

	" For each tab i
	for i in range(1, tabpagenr('$'))
		" Sets the tab page number, so that mouse clicks work
		let s ..= '%' .. i .. 'T'

		let innerText = '%{TabFilename(' .. i .. ')}%{TabReadonly(' .. i .. ')}%{TabModified(' .. i .. ')} '

		" If tab is the selected one
		if i == tabpagenr()
			" If tab isn't the left most
			if i > 1
				let s ..= '%#TLTabSelLC#%{tabcap}'
			endif

			let s ..= '%#TLTabSel# ' .. l:innerText

			" If tab is the last one, right cap bg color has to be different
			let s ..= (i == tabpagenr('$') ? '%#TLTabSelRCF#' : '%#TLTabSelRC#') .. '%{tabcap}'

		else
			let s ..= '%#TLTab#'
			" If tab isn't to the right of selected and isn't the left most 
			if (i-1 != tabpagenr()) && (i > 1)
				let s ..= '%{tabmiddlecap}'
			endif
			
			let s ..= ' ' .. l:innerText

			" Last tab has to have a tabcap
			if i == tabpagenr('$')
				let s ..= '%#TLTabRCF#%{tabcap}'
			endif
		endif
	endfor

	" After last tab, fill with TLRest
	let s ..= '%#TLRest#%T'

	return s
endfunction
