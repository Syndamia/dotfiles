nnoremap <F6> :call SpellCheckModeToggle()<CR>

function! SpellCheckModeToggle()
	if g:colors_name == 'gruvbox'
		set spell
		colorscheme darkblue
	else
		set nospell
		colorscheme gruvbox
	endif
endfunction
