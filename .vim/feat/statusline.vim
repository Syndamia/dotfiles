" Needed settings
set laststatus=2
set timeoutlen=1000 ttimeoutlen=50
set noshowmode

" Logic

let leftcap = ''
let rightcap = ''
let leftmcap = ''
let rightmcap = ''

let g:activesl   = ''
let g:inactivesl = ''

" Colors {{{
	autocmd ColorScheme * call SLCreateHighlightGroups()

	function! SLCreateHighlightGroups()
		hi SLMode  ctermfg=1 ctermbg=0
		hi SLModeC ctermfg=1 ctermbg=0

		hi SLRowCol  ctermfg=238 ctermbg=244
		hi SLRowColC ctermfg=244 ctermbg=239
	endfunction
	call SLCreateHighlightGroups()
" }}}

" Mode {{{
	" Values are, in order, for: normal (default), insert, replace, visual modes
	"	[ctermfg, ctermbg]
	let s:modecolors = [
	\	['236', '117'],
	\	['236', '119'],
	\	['236', '203'],
	\	['236', '216'],
	\]

	let g:modestring = ''

	function! SLModeSetter()
		let cm = mode()
		let ind = 0
		 
		if cm == 'i'
			let ind = 1
		elseif cm == 'R'
			let ind = 2
			let cm = 'r'
		elseif cm == 'v'
			let ind = 3
		endif

		call hlset([#{name: 'SLMode',  ctermfg: s:modecolors[l:ind][0], ctermbg: s:modecolors[l:ind][1]}])
		call hlset([#{name: 'SLModeC', ctermfg: s:modecolors[l:ind][1], ctermbg: '239'}])
		let g:modestring = l:cm

		return ''
	endfunction

	let g:activesl   ..= '%#StatusLine#%{SLModeSetter()}%#SLModeC#%{leftcap}%#SLMode#%{modestring}%#SLModeC#%{rightcap}%<'
	                   "                   \______leftcap______/\________mode_______/\______rightcap_____/
	let g:inactivesl ..= '%#StatusLineNC#   %<'
" }}}

" Filename {{{
	let g:_filename = ' %f %{rightmcap}'
	let g:activesl   ..= '%#StatusLine#'..g:_filename
	let g:inactivesl ..= '%#StatusLineNC#'..g:_filename
" }}}

" File stat {{{
	function! SLReadonly()
		return (&ft !~? 'vimfiler' && &readonly) ? ' ' : ''
	endfunction

	function! SLModified()
		return (&ft =~ 'vimfiler') ? '' : (&modified ? '' : (&modifiable ? '' : ''))
	endfunction

	let g:_filestat = ' %{SLReadonly()}%{SLModified()} '
	let g:activesl   ..= g:_filestat
	let g:inactivesl ..= g:_filestat
" }}}

" Middle separator {{{
	let g:activesl   ..= '%='
	let g:inactivesl ..= '%='
" }}}

" Character under cursor {{{
	let g:activesl   ..= ' %b '
	let g:inactivesl ..= ' %b '
" }}}

" Filetype {{{
	let g:_filetype = '%{leftmcap} %{WebDevIconsGetFileTypeSymbol()} %{&ft} '
	let g:activesl   ..= g:_filetype
	let g:inactivesl ..= g:_filetype
" }}}

" Line and column count {{{
	let g:_linecol = '%#SLRowColC#%{leftcap}%#SLRowCol#%l:%c%#SLRowColC#%{rightcap}'
	let g:activesl   ..= g:_linecol
	let g:inactivesl ..= g:_linecol
" }}}

" Statusline setting {{{
	set statusline=

	augroup SLModeAU
		au!
		au WinEnter,BufEnter * setlocal statusline=%!g:activesl
		au WinLeave,BufLeave * setlocal statusline=%!g:inactivesl
	augroup end
" }}}
