" This file must NOT have any mappings!

"""
""" ALE
"""

set omnifunc=ale#completion#OmniFunc
let g:ale_completion_enabled = 1

let g:ale_cursor_detail = 1
let g:ale_set_balloons = 1
let g:ale_hover_to_floating_preview = 1

" set ttymouse=xterm
let g:ale_floating_preview = 1 " Use floating window
let g:ale_floating_window_border = []

let g:ale_typescript_tsserver_use_global = 1 " Use global tsserver package

"""
""" DelimitMate
"""

let delimitMate_expand_cr = 1
" Don't autocomplete diamond brackets in HTML (compatibility with closetag plugin)
autocmd FileType html let b:delimitMate_matchpairs='(:),[:],{:}'

"""
""" gruvbox
"""

let g:gruvbox_contrast_dark = 'hard'

"""
""" NERDTree
"""

let NERDTreeShowHidden = 1
let NERDTreeWinPos     = "right"
let NERDTreeIgnore     = ['\.swp$', '\~$'] " Ignore file, ending with .swp and ~

" Do not save blank screens, solves https://github.com/preservim/nerdtree/issues/745
set sessionoptions-=blank

"""
""" popup_scrollbar
"""

let g:popup_scrollbar_auto = 1
let g:popup_scrollbar_shape = {
	\ 'head': '',
	\ 'body': '│',
	\ 'tail': '', }
let g:popup_scrollbar_highlight = 'Comment'

"""
""" SuperTab
"""

let g:SuperTabDefaultCompletionType = "context"

"""
""" Tabman
"""

let g:tabman_side   = 'right'

"""
""" texty-office
"""

let g:texty_office_executable_directory='/home/kamen/Programming/GitLab-repos/texty-office'
" let g:texty_office_pretty_mode=1

"""
""" Undotree
"""

let g:undotree_WindowLayout       = 2
let g:undotree_ShortIndicators    = 1 " e.g. using 'd' instead of 'days' to save some space.
let g:undotree_SetFocusWhenToggle = 1 " if set, let undotree window get focus after being opened, otherwise focus will stay in current window.
let g:undotree_TreeNodeShape      = '*'
let g:undotree_DiffCommand        = "diff"
