"""
""" Settings
"""

autocmd FileType * set foldmethod=syntax

autocmd BufRead,BufNewFile * set foldtext=MyFoldText()
function! MyFoldText()
	"  Gets the first fold line and replace tabs with spaces (as many as shiftwidth is set to)
	let line      = substitute(getline(v:foldstart), "\t", repeat(" ", shiftwidth(0)), "")

	"  Calculates amount of folded lines
	let linecount = v:foldend - v:foldstart

	"  Shows our line, then a lot of spaces, and at the very end we have line number and arrows
	return line . repeat(" ", winwidth('%') - strlen(line) - 10 - strlen(linecount))
	          \ . "  "  . linecount .  "  "
endfunction

" autocmd BufEnter * call ResizeOnEnter()
function! ResizeOnEnter()
	vert res &columns/2
	hor  res &lines*2/3

	for bufN in range(1, bufnr('$'))
		if bufname(bufN) =~ "NERD_tree"
			call setbufvar(bufN, "&winwidth", 31)
		endif
	endfor
endfunction

"""
""" Config file formatting
"""

autocmd FileType vim,sh,zsh,xdefaults setlocal foldlevel=0

"""
""" Programming language formatting
"""

autocmd FileType css,ts setlocal ts=2 sw=2 sts=0 expandtab " Transform tabs in CSS and TS into 2 spaces

autocmd FileType lisp,scheme,haskell setlocal ts=2 sw=2 sts=0 expandtab
autocmd BufRead,BufNewFile *.component.css set filetype=css
