"""
""" Runtimes
"""

runtime plugins_list.vim
runtime plugins_conf.vim

runtime mappings.vim
runtime autocommands.vim

runtime feat/code_terminal.vim
runtime feat/spell_check_mode.vim
runtime feat/statusline.vim
runtime feat/tabline.vim
runtime feat/term_scroll.vim

"""
""" Settings
"""

""
"" Colors
""

colorscheme gruvbox
set background=dark

syntax on
filetype plugin indent on
set nohlsearch

""
"" Cursor
""

set scrolloff=5 " Lines above and below cursor
set showmatch   " Jump to opening brace

let &t_SI = "\e[5 q" " thin cursor on insert mode
let &t_SR = "\e[4 q" " underline on replace mode
let &t_EI = "\e[1 q" " block on normal mode

" Highlight current line but not in insert mode
set cul
autocmd InsertEnter,InsertLeave * set cul!

""
"" Characters and keys
""

set list " Customize white-space characters
set listchars=tab:│\ ,extends:>,precedes:<
set tabstop=4 " Show tabs as 4 spaces
set shiftwidth=4 " Indent with 4 spaces

set backspace=indent,eol,start
set mouse=a

""
"" Folding
""

set foldlevel=99
hi Folded ctermfg=NONE

""
"" Splitting
""

set splitbelow splitright

""
"" History
""

set history=1000
set undofile
set undodir=~/.vim/vimundo

""
"" Menus
""

set number
set signcolumn=number
set wildmenu
set wildmode=list:longest,full
set shortmess=acTOI

""
"" Spelling
""

set langmap=АA,аa,БB,бb,ВW,вw,ГG,гg,ДD,дd,ЕE,еe,ЖV,жv,ЗZ,зz,ИI,иi,ЙJ,йj,КK,кk,ЛL,лl,МM,мm,НN,нn,ОO,оo,ПP,пp,РR,рr,СS,сs,ТT,тt,УU,уu,ФF,фf,ХH,хh,ЦC,цc,Ч~,ч`,Ш{,ш[,Щ},щ],ЪY,ъy,ЬX,ьx,Ю\|,ю\\,ЯQ,яq
set spelllang=en,bg_BG                " Adds Bulgarian to spelling languages

""
"" ToHTML
""

let g:html_line_ids = 1
let g:html_dynamic_folds = 1

""
"" Other
""

autocmd BufRead,BufNewFile * set tw=0 " Sets textwidth to 0 for all files (set with autocmd since just doing "set tw=0" can be overridden)

set showcmd
set incsearch
