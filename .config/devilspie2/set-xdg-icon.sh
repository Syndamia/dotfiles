#!/bin/bash
# SOURCE: https://gist.github.com/SamAdamDay/54bc739131cc98f8d226
# Requirements: xseticon, pyxdg, xprop

# The application files to search through
SEARCH_FILES="$HOME/.local/share/applications/*.desktop /usr/share/applications/*.desktop /usr/local/share/applications/*.desktop"

# The name of this script
ME=`basename $0`

# Print help message and exit
help()
{
	echo "usage: $ME xid"
	echo "Assigns an XDG icon (specified by the current theme) to the window given by xid"
	echo
	exit 0
}

# Check for options
while getopts h OPT; do
	case "${OPT}" in
		h) help ;;
	esac
done
shift $(($OPTIND-1))

# If no id is given, then print usage
if [ "$#" == "0" ]; then help; fi

# Determine the xid
xid=$1

# Determine the WM_CLASS of the window, if possible
result=`xprop -id "$xid" WM_CLASS`

# If the xprop returned a nonzero exit status, then we need to exit too
if [ "$?" != "0" ]; then
	echo "E: Could not get WM_CLASS for the window with xid $xid. Either it doesn't exist, or it doesn't have WM_CLASS set."
	exit 1
fi

# Extract the first WM_CLASS
class=`echo "$result" | sed 's/^WM_CLASS(STRING) = "\([a-zA-Z0-9_-]\+\)".*$/\1/'`

# Find a .desktop file with StartupWMClass=$class, if one exists
file=`grep --files-with-matches --no-messages "StartupWMClass[[:space:]]*=[[:space:]]*$class" $SEARCH_FILES`

# If too many files (ie more than one) were found, then exit
if [[ "$file" == *\n*\n ]]; then
	echo "E: More than one matching .desktop file for the window with xid $xid (WM_CLASS: '$class')"
	exit 1
fi

# If we couldn't find a file, then exit
if [ ! -e "$file" ]; then
	echo "E: Could not find a matching .desktop file for the window with xid $xid (WM_CLASS: '$class')"
	exit 1
fi

# Extract the icon name from the .desktop file
icon=`grep '^Icon[[:space:]]*=.*$' "$file" | sed 's@^Icon *= *\([a-zA-Z0-9./_ (),-]\+\)$@\1@'`

# If $icon is a PNG that exists then use that, otherwise determine the icon file from the theme
if [ -e $icon ] && [ "${icon##*.}" == "png" ]; then

	iconfile=$icon

else

	# Determine the current icon theme
	theme=`gsettings get org.gnome.desktop.interface icon-theme` # Has quotes

	# Determine the icon file using the XDG Icon File Specification (http://standards.freedesktop.org/icon-theme-spec)
	# Try various sizes, preferring the larger ones
	for i in 256 128 64 32; do
		iconfile=`python -c "from xdg import IconTheme; print(IconTheme.getIconPath('$icon',$i,$theme,['png']))"`
		if [ -e "$iconfile" ]; then break; fi
	done

	# If this has failed, then $icon probably doesn't have an icon file
	if [ ! -e $iconfile ]; then 
		echo "E: Could not find icon file for icon name '$icon'. Window xid: $xid, WM_CLASS: '$class'"
		exit 1
	fi

fi

# Finally, set the the window icon to $iconfile
xseticon -id "$xid" "$iconfile"

exit 0
