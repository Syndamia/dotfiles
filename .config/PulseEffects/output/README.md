# PulseEffects Presets

Here are my custom PulseEffect presets. For more, I can recommend [JackHack96's collection](https://github.com/JackHack96/PulseEffects-Presets).

## Bass Boosted - Calmer

This is a modification of [JackHack96's "Bass Boosted" preset](https://github.com/JackHack96/PulseEffects-Presets/blob/master/Bass%20Boosted.json). It makes the bass a bit less pronounced and normalizes the mid and high ends.

**Note:** I tweaked this preset for my pair of `Sennheiser HD 280 PRO`. The original "Bass Boosted" preset is too bassy, even for them (and their bass roll-off is quite pronounced). If you have headphones with similar sound profile, you might enjoy this preset.
