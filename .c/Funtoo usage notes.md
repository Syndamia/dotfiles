## Configs

#### Default browser
[https://askubuntu.com/a/1076085/1085672]( https://askubuntu.com/a/1076085/1085672)
- `xdg-settings set default-web-browser chromium-browser.desktop`

#### Custom repo
[https://linuxreviews.org/HOWTO_make_your_own_Gentoo_ebuild](https://linuxreviews.org/HOWTO_make_your_own_Gentoo_ebuild)
- setup
```
mkdir -p /usr/local/portage

```
add `PORTDIR_OVERLAY=/usr/local/portage` to `/etc/portage/make.conf`
- The folder structure in your overlay folder should be the same as found in /usr/portage so if you wanted to make an ebuild for a theme you would put that in /usr/local/portage/x11-themes.
```
// copy files to /usr/local/portage/CATEGORY_FOLDER/PACKAGE_NAME_FOLDER/PACKAGE.ebuild

# ebuild /usr/local/portage/CATEGORY_FOLDER/PACKAGE_NAME_FOLDER/PACKAGE.ebuild digest
```

#### Installing electron apps (example with Min)
```
sudo emerge -q nodejs && sudo npm install -g npm
npm install && npm run build && npm exec electron-packager . && sudo sh -c 'mv ./Min-linux-x64 /opt && ln -s /opt/Min-linux-x64/Min /usr/bin/min'
```

#### Winamp on Adacious
[https://hisham.hm/2011/04/23/audacious-with-classic-winamp-1x-skin/](https://hisham.hm/2011/04/23/audacious-with-classic-winamp-1x-skin/) [https://ubuntugeneration.wordpress.com/2009/06/30/audacious-player-with-winamp-skin/ ](https://ubuntugeneration.wordpress.com/2009/06/30/audacious-player-with-winamp-skin/) [https://www.gnome-look.org/p/1008363](https://www.gnome-look.org/p/1008363)
- download and unzip the skin (last link) and then do
```
mkdir -p ~/.local/share/audacious/Skins
mv Winamp ~/.local/share/audacious/Skins
```

#### libinput config
[https://wiki.gentoo.org/wiki/Libinput#Touchpad_tap-to-click](https://wiki.gentoo.org/wiki/Libinput#Touchpad_tap-to-click)
- `/etc/X11/xorg.conf.d/40-libinput.conf` Adding tap-to-click
```
Section "InputClass"
     Identifier "libinput touchpad catchall"
     MatchIsTouchpad "on"
     MatchDevicePath "/dev/input/event*"
     Option "Tapping" "True"
     Option "TappingDrag" "True"
     Driver "libinput"
EndSection
```
- `/etc/X11/xorg.conf.d/40-libinput.conf` Adding natural scrolling
```

Section "InputClass"
     Identifier "libinput touchpad catchall"
     MatchIsTouchpad "on"
     MatchDevicePath "/dev/input/event*"
     Option "NaturalScrolling" "True"
     Driver "libinput"
EndSection
```

#### Dotnet
[https://wiki.gentoo.org/wiki/Eselect/Repository](https://wiki.gentoo.org/wiki/Eselect/Repository)
```
# emerge --ask app-eselect/eselect-repository
# eselect repository enable dotnet
# emerge --sync
```

#### My Epson Scanner
1. Download and extract https://filite.syndamia.com/f/eps2bu
2. Have rpm installed
3.
```
sudo rpm -Uvh --nodeps ./epsonscan2-bundle-6.6.2.4.x86_64.rpm/core/epsonscan2-6.6.2.4-1.x86_64.rpm
sudo rpm -Uvh --nodeps ./epsonscan2-bundle-6.6.2.4.x86_64.rpm/plugins/epsonscan2-non-free-plugin-1.0.0.4-1.x86_64.rpm
```

## Fixes

#### chroot: No such file or directory (lubuntu)
[https://forums.funtoo.org/topic/864-is-ubuntu-1404-lts-a-good-live-cd-for-a-funtoo-install/?do=findComment&comment=4298](https://forums.funtoo.org/topic/864-is-ubuntu-1404-lts-a-good-live-cd-for-a-funtoo-install/?do=findComment&comment=4298)
```
# /usr/bin/env -i HOME=/root TERM=$TERM /usr/sbin/chroot . bash -l
```

#### delete partition after mkfs
[https://superuser.com/a/1281363/1258400](https://superuser.com/a/1281363/1258400) [https://linux.die.net/man/8/wipefs](https://linux.die.net/man/8/wipefs)
- `# wipefs --all /dev/DEVICE`


#### Failed to load /dev/dri/card0
[https://forums.funtoo.org/topic/2904-failed-to-load-devdricard0/?do=findComment&comment=12821](https://forums.funtoo.org/topic/2904-failed-to-load-devdricard0/?do=findComment&comment=12821)
- get rid of the nomodeset in /etc/boot.conf      re-run `ego boot update` and reboot

#### grub2-install: "this GPT partition label contains no BIOS Boot Partition"
[https://superuser.com/a/1610045/1258400](https://superuser.com/a/1610045/1258400)
- You already have this tiny partition, so kk the first partition starts not with 1 but with 2048

```
/dev / sda1 2048 411647 409600 200M Linux file system (/boot)
```
You need to change the label type, from boot to bios_grub for example through parted:
```
parted /dev/sda
set 1 boot off
set 1 bios_grub on
q
```
Then again:
```
grub2-install /dev/sda
```

#### Audacious fix
- write (to turn on the arch)
  ```
  media-sound/audacious **
  media-plugins/audacious-plugins **
  ```
  into /etc/portage/package.accept_keywords/audacious
- add the gentoo_ice-wmms line from [https://gitweb.gentoo.org/repo/gentoo.git/tree/media-sound/audacious/Manifest](https://gitweb.gentoo.org/repo/gentoo.git/tree/media-sound/audacious/Manifest) into `/var/git/meta-repo/kits/media-kit/media-sound/audacious/Manifest`

#### Dunst dbus session
[https://www.reddit.com/r/artixlinux/comments/n4vm53/how_to_get_notifications_to_work_with_dunst/](https://www.reddit.com/r/artixlinux/comments/n4vm53/how_to_get_notifications_to_work_with_dunst/)
> Adding these lines to .xinitrc fixed it
> 
> ```
> if which dbus-launch >/dev/null && test -z "$DBUS_SESSION_BUS_ADDRESS"; then
>     eval "$(dbus-launch --sh-syntax --exit-with-session)"
> fi
> ```
> Test it with echo "$DBUS_SESSION_BUS_ADDRESS". If you get a directory as output, then you have a dbus session id for these programs to use.

#### polkit / consolekit
[https://forums.gentoo.org/viewtopic-t-1051966-start-0.html](https://forums.gentoo.org/viewtopic-t-1051966-start-0.html)
- run as root and reboot
```
rc-update add consolekit
rc
rc-update show
rc-status
```

#### Run loginctl as user
[https://www.linuxquestions.org/questions/slackware-14/slackware-current-is-possible-to-run-loginctl-hibernate-as-user-4175696586/#post6263112](https://www.linuxquestions.org/questions/slackware-14/slackware-current-is-possible-to-run-loginctl-hibernate-as-user-4175696586/#post6263112)
```
cat /etc/polkit-1/rules.d/88-hibernate.rules
polkit.addRule(function(action, subject) {
    if (action.id == "org.freedesktop.login1.hibernate" &&
        subject.isInGroup("power")) {
        return polkit.Result.YES;
    }
});
```

#### ElectronMail "No native build was found"
Package the app, even if you can't do it all the way. So just run
```
npm run electron-builder:dist:linux:rpm
```
even if it gives an error, the app should be working. To test, do `npm run start:electron`

#### clang undefined reference to `setupterm' ninja -v -j4 -l0 distribution failed
[https://github.com/dougpuob/cppnamelint/issues/24](https://github.com/dougpuob/cppnamelint/issues/24)
Rebuild llvm without ncurses flag

### circular dependencies
[https://forums.gentoo.org/viewtopic-t-1078738-start-0.html](https://forums.gentoo.org/viewtopic-t-1078738-start-0.html)
Try to emerge each one with `--nodeps`.

### piper and ratbagctl can't connect to ratbagd
[https://github.com/libratbag/piper/issues/559#issue-677712902](https://github.com/libratbag/piper/issues/559#issue-677712902)
`sudo usermod -aG games USERNAME` and reboot (logout is not enough!).

### libreoffice --draw and --impress show "loading component library libsdlo.so failed"
[https://bugs.funtoo.org/browse/FL-8035](https://bugs.funtoo.org/browse/FL-8035)
Emerge `app-office/libreoffice-bin` with the `zeroconf` flag. Potentially beforehand do `emerge -C app-office/libreoffice-bin && rm -rf /usr/lib64/libreoffice/`.
If that doesn't work, try installing libavahi. Draw doesn't use it and Impress uses it for Remote Control.

[https://forum.slitaz.org/topic/libreoffice-problems-libsdloso](https://forum.slitaz.org/topic/libreoffice-problems-libsdloso)

Only if **both** fail, you could try making a "blank" libavahi:
```
echo 'int avahi_client_new(void) { return 0; }' > libavahi-client.c
echo 'int avahi_threaded_poll_new(void) { return 0; }' > libavahi-common.c
gcc -c libavahi-client.c libavahi-common.c
gcc -shared -o libavahi-client.so.3 libavahi-client.o
gcc -shared -o libavahi-common.so.3 libavahi-common.o
cp libavahi-*.so.3 /usr/lib64/libreoffice/program/
```

## Resources

#### Install
[https://www.funtoo.org/Install](https://www.funtoo.org/Install)
- [https://packages.gentoo.org/categories/x11-drivers](https://packages.gentoo.org/categories/x11-drivers)
- [https://wiki.gentoo.org/wiki/NetworkManager#OpenRC](https://wiki.gentoo.org/wiki/NetworkManager#OpenRC) [https://computingforgeeks.com/use-virt-manager-as-non-root-user/](https://computingforgeeks.com/use-virt-manager-as-non-root-user/)

#### Track download status of portage stuff
`tail -f /var/log/emerge-fetch.log` `tail -f /var/tmp/portage/dev-qt/qtwebengine-5.15.5/temp/build.log`
#### How long package build took
`sudo splat PACKAGE_NAME`

#### Fingerprint unsuppported devices list
[https://gitlab.freedesktop.org/libfprint/wiki/-/wikis/Unsupported-Devices](https://gitlab.freedesktop.org/libfprint/wiki/-/wikis/Unsupported-Devices)

#### Funtoo tlp
[https://github.com/dywisor/tlp-portage](https://github.com/dywisor/tlp-portage)

#### Power management guide
[https://wiki.gentoo.org/wiki/Power_management/Guide](https://wiki.gentoo.org/wiki/Power_management/Guide)

#### Have atleast 32GB of RAM+Swap!
[https://bugs.funtoo.org/browse/FL-7012?jql=text%20~%20%22subcommand%20failed%22](https://bugs.funtoo.org/browse/FL-7012?jql=text%20~%20%22subcommand%20failed%22)
