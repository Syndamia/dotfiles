|About|Description|Link|
|---|---|---|
|***Commands***|||
|Fedora/GRUB|Fedora update grub command|[zeldor.biz](https://zeldor.biz/2013/02/fedora-update-grub/)|
|Fedora|A good list of useful dnf commands (fedora package manager)|[rootusers](https://www.rootusers.com/25-useful-dnf-command-examples-for-package-management-in-linux/)|
|Fedora|Get currently pressed key names|[superuser](https://superuser.com/a/1000765)|
|SELinux|Disable SELinux permanently or temporarely|[linuxconfig](https://linuxconfig.org/how-to-disable-selinux-on-linux)|
|***Bug fixes***|||
|Dotnet|Fix for `dotnet new` instance of an object error|[github issues](https://github.com/dotnet/templating/issues/2189#issuecomment-568675397)|
|Fedora/Dotnet|Fix of many dotnet issues on fedora (install dotnet packages from microsoft repo, not from fedora repo)|[github issues](https://github.com/OmniSharp/omnisharp-vscode/issues/4360#issuecomment-779086868)|
|Fedora/Dotnet|Fix of missing `compat-openssl-10` when trying to install dotnet (5) on Fedora 34|[github issues](https://github.com/OmniSharp/omnisharp-vscode/issues/4360#issuecomment-829013522)|
|Fedora/Kernel|Potential fix of not restored system after hibernation|[bugzilla](https://bugzilla.redhat.com/show_bug.cgi?id=1795422#c5)|
|Fedora/Kernel|Fix for "A stop job is running for Session 1" after shutdown|[bugzilla](https://bugzilla.redhat.com/show_bug.cgi?id=1088619#c46)|
|Wireless|How to turn off wireliess powermanagement (fixed slow bootup time)|[unix stackexchange](https://unix.stackexchange.com/a/315400/416793)|
|***Configurations***|||
|Fedora|Turn off wireless power management via TLP|[unix stackexchange](https://unix.stackexchange.com/a/473753/416793)|
|Fedora/Samba|Simplest way to setup samba on fedora|[ask fedora](https://ask.fedoraproject.org/t/how-to-setup-samba-on-fedora-the-easy-way/2551/24)|
|Openbox/PulseAudio|Most correct way to start pulseaudio on openbox launch|[freedesktop wiki](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Startup/#openbox)|
|PulseAudio|pulseaudio's built in equalizer **(use pulseeffects instead)**|[freedesktop wiki](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Modules/#module-equalizer-sink)|
|PulseAudio|qpaeq: pulseaudio equalizer gui|[freedesktop wiki](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Equalizer/#gettingtheguipqaeq:)|
