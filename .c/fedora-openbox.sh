#!/bin/bash

# A small and badly written script I use for installing most of what I need in my Openbox installation setup.
# It's meant to be used while having an installation without any desktop environment (fedora netinstaller - minimal).

sudo dnf install -y openbox xbacklight feh conky xorg-x11-drv-libinput tint2 volumeicon xorg-x11-server-utils network-manager-applet terminator
sudo dnf install -y xorg-x11-server-common
sudo dnf install -y xinit

sudo dnf install -y xdm
sudo systemctl enable xdm
echo "exec openbox-session" > .xsession 
chmod +x .xsession
sudo systemctl set-default graphical.target

sudo dnf install -y xrdb
sudo dnf install -y xorg-x11-drv-*
sudo dnf install -y sessreg

sudo dnf install -y jgmenu
sudo dnf install -y arandr

mkdir Downloads
cd Downloads
    git clone https://github.com/Leinnan/lwa-hot-corners.git
    make
    sudo make install
cd ~

sudo dnf install -y cinnamon-themes
sudo dnf install -y gnome-terminal
sudo dnf install -y i3lock xautolock

cd Downloads
    git clone https://github.com/xkbmon/xkbmon.git
    make
    sudo cp xkbmon /bin
cd ~
