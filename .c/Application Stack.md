## Application Stack

|Type|Main app|Backup|Alternative|
|---|---|---|---|
|**Desktop environment**|||
|Window manager|openbox|||
|File manager|nemo|||
|Application launcher|jgmenu|||
|Desktop panel|tint2|||
|Compositor|picom|||
|Session manager|?|||
|Display manager|lightdm|||
|||||
|**System**||||
|System monitor|GNOME System Monitor|htop||
|System restore|Timeshift|||
|Terminal|Terminator|XTerm|||
|Audio controller|pavucontrol|alsamixer||
|||||
|**Internet**||||
|Browser|Brave, Rambox||Firefox, Badwolf|
|Email|Claws Mail, ElectronMail|Sylpheed||
|Messaging|Discord, Jabber|||
|||||
|**Multimedia**||||
|Music|mocp, Audacious|||
|Video player|mpv|||
|Video recorder|OBS Studio||peek|
|Image viewer|Eye of Gnome|||
|Vector graphics viewer|Inkscape|||
|||||
|**Office**||||
|Office Suite|LibreOffice|||
|PDF Viewer|Evince|||
|TeX editor|TexMaker|||
|||||
|**Editing**||||
|Text|vim|||
|Video|Blender|||
|Images|GIMP, Inkscape|||
|**Tray**||||
|Screenshot utility|Flameshot|||
|Clipboard manager|CopyQ|||
|Internet applet|nmapplet|||
|Volume applet|pnmixer|||
|Color temperature|redshift|||
