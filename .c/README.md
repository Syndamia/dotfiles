# dotfiles

My home folder is a git repository, where all files are excluded (with exceptions of course). Having it always be a git repository brings in some issues, so whenver I need to "sync", I rename `.git.disabled` to `.git`, and when I'm finished, I do the opposite (`.a/toggle-repo.sh`).

## Specific folders

- `.a`: All of my scripts
- `.b`: Backups of configs, who aren't in home folder
- `.c`: Documents and notes regarding my setup
