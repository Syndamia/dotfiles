#!/bin/bash

mkdir -p "update-$(date)"
mv *.zsh "update-$(date)"

wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/lib/compfix.zsh
wget https://github.com/ohmyzsh/ohmyzsh/raw/master/lib/history.zsh
wget https://github.com/ohmyzsh/ohmyzsh/raw/master/lib/key-bindings.zsh
wget https://github.com/ohmyzsh/ohmyzsh/raw/master/lib/spectrum.zsh
wget https://github.com/ohmyzsh/ohmyzsh/raw/master/lib/termsupport.zsh
