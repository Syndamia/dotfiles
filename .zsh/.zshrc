device='desktop' # can be either "desktop" (default when empty), "server" or "android"

#
# Environment variables
#

export PATH=$HOME/bin:/usr/local/bin:$PATH # Adds ~/bin and /usr/local/bin paths to PATH
export PATH="$PATH:$HOME/.npm-global/bin"  # Adds ~/.npm-global/bin path to PATH
export PATH="$PATH:$HOME/go/bin"           # Adds ~/go/bin to PATH
export PATH="$PATH:/sbin"
export FPATH="$FPATH:$HOME/.zsh/completion"

export EDITOR='vim'

#
# Options
#

stty -F/dev/tty -ixon -ixoff # Disables Software Flow Control
setopt long_list_jobs

# Changing/making/removing directory
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushdminus

# recognize comments
setopt interactivecomments

# don't backward-kill-word on forward slash
autoload -U select-word-style
select-word-style bash

# enable (new?) zsh completion system
# Thanks https://github.com/reo101/dotfiles/blob/master/home/private_dot_config/private_zsh/private_dot_zshrc
zdumppath="$HOME/.zsh/zcompdump"
autoload -U compinit
# Cache completion if nothing changed - faster startup time
typeset -i updated_at=$(date +'%j' -r "$zdumppath" 2>/dev/null || stat -f '%Sm' -t '%j' "$zdumppath" 2>/dev/null)
if [ $(date +'%j') != ${updated_at} ]; then
	compinit -d "$zdumppath" -i
else
	compinit -d "$zdumppath" -C -i
fi

#
# Binds
#

bindkey '^H' backward-kill-word

#
# Theme
#

ZSH_THEME="gruvbox"
SOLARIZED_THEME="dark"

source ~/.zsh/homelander/homelander.zsh
source ~/.zsh/homelander/homelander-sections.zsh

if [ "$USER" = 'kamen' ]; then
	export PROMPT='$(hl_exitpwd)$(hl_cecho_caps $(hl_user)  ) $(hl_cecho $(hl_precursor)) '
	HL_USER_T='%D{%H:%M}'
else
	export PROMPT='$(hl_exitpwd)$(hl_cecho_caps $(hl_user)  ) $(hl_cecho $(hl_time)) $(hl_cecho $(hl_precursor)) '
fi
export RPROMPT='$(hl_duration)'
[ "$NIX_SHELL" = 'y' ] && HL_USER_BG=202

hl_hooks() {
	hl_time_async
	hl_precursor_async
}

# Custom section
hl_exitpwd() {
	__bg=$([ -n "$SSH_TTY" ] && echo $HL_USER_SSH || echo $HL_USER_BG)

	line=(' |' ' ' ']\n' '\n' '\b' '\b' 0)
	[ "$_exit_code" -ne 0 ] && line+=(o $HL_EXIT_FG ' %? ')
	[ "$_print_directory" -ne 0 ] && line+=(o $__bg ' %d ')

	[ ${#line} -gt 7 ] && hl_concatsec $line
}
hl_duration() {
	local __dur=($(hl_exec_duration))

	[ "${#__dur}" -gt 0 ] && hl_cecho_caps o 140 " ${__dur[*]:2} "     \
						  || true
}

#
# Plugins
#

source ~/.zsh/omz/history.zsh
source ~/.zsh/omz/termsupport.zsh
source ~/.zsh/omz/key-bindings.zsh
source ~/.zsh/omz/compfix.zsh

source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#
# Custom functions
#

if [ -n "$ZSHRC_FUNTOO" ]
then
	pre-sync() {
		echo '(.zshrc) Running pre-sync function...'

		# Since in post-sync I change the EAPI of a lot of the ebuilds,
		# the overlay can't be synced properly
		_pwd=$(pwd)
		cd /var/db/repos/gentoo
		sudo git reset --hard
		cd $_pwd

		echo '(.zshrc) pre-sync finished! :}'
	}

	post-sync() {
		echo '(.zshrc) Running post-sync function...'

		# Fixes the elusive
		# "!!! Repository name or alias 'gentoo', defined for repository 'core-kit', overrides existing alias or repository."
		# error when enabling gentoo overlay
		sudo sed -i 's/aliases = gentoo/#aliases = gentoo/g' /var/git/meta-repo/kits/core-kit/metadata/layout.conf

		# In the gentoo overlay, most packages don't actually use EAPI 8 specific features, so
		# they can safely be rolled back to EAPI 7 (funtoo's portage doesn't support EAPI 8 yet).
		# However, there are still some that do need EAPI 8, so they should be left alone.
		# Since I'm incredibly lazy, for now, this is how I'm gonna do it.
		find '/var/db/repos/gentoo' -type f -name "*.ebuild" ! -path "*dev-qt*" ! -path "*kde-frameworks*" ! -path "*kde-plasma*" \
		| xargs sudo sed -i 's/EAPI=8/EAPI=7/g'

		echo '(.zshrc) post-sync finished! :]'
	}

	alias sen="pre-sync && sudo ego sync && post-sync && sudo emerge --quiet-build -vuDN @world"
fi

# plumb.sh
plumb-store-cwd () {
	pwd > /tmp/plumb-cwd
}
zle -N plumb-store-cwd
bindkey "^[[5;7~" plumb-store-cwd

# Run in the background
bgr() {
	nohup sh -c "$@" 2>&1 &
	disown
}

# Kill process and run in the background
res() {
	pkill $@
	bgr $@
}

# Open vim with .vim-session file, if it exists and the vim command doesn't have any parameters
v() {
	if [ -f ".vim-session" ] && [ -z "$1" ]; then
		vim -S .vim-session
	else
		vim $@
	fi
}

# Add to grit parent, where first value is parent number and everything else is node value
grap() {
	parent="$1"
	shift
	grit add -p $parent -- $@
}

onemo() {
	bgr "nemo ./"
}

# Launch .desktop application (either full path or just name)
dela() {
	name=$1
	if [[ ${name:0:1} != "/" ]]; then
		name="/usr/share/applications/${name}"
	fi
	if [[ ! $name =~ .desktop$ ]]; then
		name+=".desktop"
	fi
	comm=($(awk -F= '$1=="Exec"{$1=""; print}' "$name"))
	bgr "$comm"
}

metas() {
	for file in *; do
		[ "$file" != 'cover.jpeg' ] && \
			kid3-cli -c "set title \"$(echo "$file" | sed -e "s/\.[^.]*$//")\"" \
					 -c "set artist \"$1\"" -c "set album \"$2\"" -c "set date \"$3\"" \
					 -c 'set picture:"./cover.jpeg" ""' "$file" \
			|| :
	done
}

#
# Aliases
#

if [ -x "$(command -v emerge)" ]
then
	alias seq="sudo emerge --quiet-build -v"
	alias sep="sudo emerge -pv"
	alias ses="sudo emerge -s"
	alias seS="sudo emerge -S"
	alias senc="sudo emaint sync -A"
	alias sen="sudo emaint sync -A && sudo emerge --quiet-build -vuDN @world"
	alias senp="sudo emaint sync -A && sudo emerge --quiet-build -pvuDN @world"
	alias seN="sudo emerge --quiet-build -vuDN @world"
	alias seNp="sudo emerge --quiet-build -pvuDN @world"
	sedi() {
		sudo emerge --deselect "$1" && sudo emerge --depclean "$1"
	}

	alias use="sudo vim /etc/portage/package.use"
	alias unmask="sudo vim /etc/portage/package.unmask"
	alias mask="sudo vim /etc/portage/package.mask"
fi

if [ -x "$(command -v dnf)" ]
then
	alias sdi="sudo dnf install -y"
	alias sdr="sudo dnf remove -y"
	alias sda="sudo dnf autoremove -y"
	# If you're using dotnet from the "packages-microsoft-com-prod" repo, make sure to
	# add "exclude=dotnet* aspnetcore* netstandard*" inside the /etc/yum.repos.d/fedora.repo, /etc/yum.repos.d/fedora-updates.repo and potentially /etc/yum.repos.d/fedora33.repo
	alias sdu="sudo dnf upgrade -y && sudo dnf autoremove -y && sudo youtube-dl --update && wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash && needs-restarting -r"
	alias sdudiscord="wget -O discord.tar.gz \"https://discord.com/api/download?platform=linux&format=tar.gz\" && tar -xzf discord.tar.gz && sudo rm -rf /opt/Discord && sudo mv Discord /opt"

	alias ds="dnf search"
	alias dcs="dnf copr search"
	alias dp="dnf provides"
	alias di="dnf info"

	alias sdce="sudo dnf copr enable -y"
	alias sdcei='f() { sudo dnf copr enable -y $1 && sudo dnf install -y $2; unset -f f }; f'
fi

if [ -x "$(command -v apt-get)" ]; then
	alias sasy="sudo apt install -y"
	alias sary="sudo apt remove -y"
	alias saty="sudo apt autoremove -y"
	alias sagt="sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y && sudo youtube-dl --update && wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash"
fi

if [ -x "$(command -v exa)" ]
then
	alias ll="exa --icons --group-directories-first --git --time-style long-iso -a -labh"
	alias lk="exa --icons --group-directories-first --git --time-style long-iso -labhTL"
	alias l.="exa --icons --group-directories-first --git --time-style long-iso -labhTL 2"
	alias l="exa --icons --group-directories-first --time-style long-iso -abh"
else
	alias ll="la -alF"
	alias la="ls -A"
	alias l="ls -CF"
fi

if [ -x "$(command -v grit)" ]
then
	alias gr="grit"
	alias gra="grit add --"
	alias grc="grit check"
	alias grls="grit tree"
fi

if [ -x "$(command -v youtube-dl)" ]
then
	alias ydl="youtube-dl -o '%(title)s.%(ext)s'"
	alias ydlba="youtube-dl -o '%(title)s.%(ext)s' --audio-format best -x"
fi

if [ -x "$(command -v combinepdf)" ]
then
	alias lg="lazygit"
fi

if [ -x "$(command -v gs)" ]
then
	alias combinepdf="gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=temp.pdf"
fi

cdrepo () {
	[ -n "$2" ] && cd "$HOME/Programming/$1/$2" \
				|| cd "$HOME/Programming/$1"
}
alias cdg="cdrepo GitHub-repos"
alias cdl="cdrepo GitLab-repos"
alias cds="cdrepo source"

alias wudo="sudo -u www-data"

alias q="exit"
alias x="exit"

#
# Added by scripts:
#

export PATH="/home/kamen/bin/Sencha/Cmd:$PATH"

export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
