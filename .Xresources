xlogin*geometry: 300x300-0-0

!! Xterm

xterm*termName: xterm-256color
xterm*saveLines: 16384
! Fixes Ctrl+Backspace not working with .vimrc
! Thanks to https://unix.stackexchange.com/a/249824/416793
! XTerm*ptyInitialErase: true
XTerm*backarrowKey: false

! Font

xterm*font: xft:MesloLGS NF:pixelsize=15:antialias=true
xterm*renderFont: true

Xft.dpi: 96

xterm*cursorBlink: true
xterm*cursorOnTime: 500
xterm*cursorOffTime: 500

xterm*loginShell: true


! Turn the scrollbar on, and put it on the right
xterm*scrollBar: true
xterm*rightScrollBar: true

xterm*mouseWheelScrollPage: true

xterm*selectToClipboard: true
xterm*VT100.Translations: #override \
    Ctrl <Key> minus: smaller-vt-font() \n\
    Ctrl <Key> plus: larger-vt-font() \n\
    Ctrl <Key> 0: set-vt-font(d)

! Gruvbox Theme {{{

! -----------------------------------------------------------------------------
! File: gruvbox-dark.xresources
! Description: Retro groove colorscheme generalized
! Author: morhetz <morhetz@gmail.com>
! Source: https://github.com/morhetz/gruvbox-generalized
! Last Modified: 6 Sep 2014
! -----------------------------------------------------------------------------
 
! hard contrast: *background: #1d2021
*background: #282828
! soft contrast: *background: #32302f
*foreground: #ebdbb2
! Black + DarkGrey
*color0:  #282828
*color8:  #928374
! DarkRed + Red
*color1:  #cc241d
*color9:  #fb4934
! DarkGreen + Green
*color2:  #98971a
*color10: #b8bb26
! DarkYellow + Yellow
*color3:  #d79921
*color11: #fabd2f
! DarkBlue + Blue
*color4:  #458588
*color12: #83a598
! DarkMagenta + Magenta
*color5:  #b16286
*color13: #d3869b
! DarkCyan + Cyan
*color6:  #689d6a
*color14: #8ec07c
! LightGrey + White
*color7:  #a89984
*color15: #ebdbb2

! }}}
