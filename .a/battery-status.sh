#!/bin/bash

# Check for dependencies
for comm in upower awk; do
	command -v upower >/dev/null 2>&1 || (echo "Error, missing command dependencies: $comm" && exit)
done

# Prepare battery informatoin
for bat in BAT0 BAT1; do
	upower -i "/org/freedesktop/UPower/devices/battery_$bat" > "/tmp/$bat"
	[ "$(awk '/native-path/{print $2}' "/tmp/$bat")" = '(null)' ] && continue

	bat_state="$(awk '/state/{print substr($2, 1, 1)}' "/tmp/$bat")"
	if   [ $bat_state = 'c' ]; then bat_power_ind=''
	elif [ $bat_state = 'd' ]; then bat_power_ind=''
	else                            bat_power_ind=''
	fi

	bat_per="$(awk '/percentage/{print strtonum($2)}' "/tmp/$bat")"
	# bat_per=$((bat_per * 5 / 4 - 25)) # compensation for failing battery

	[ $bat_state = 'd' ]                                                        \
	&& bat_time="$(awk '/time to empty/{print $4 substr($5, 1, 1)}' "/tmp/$bat")" \
	|| bat_time="$(awk '/time to full/ {print $4 substr($5, 1, 1)}' "/tmp/$bat")"

	echo -n " $bat_power_ind ${bat_per}% $bat_time"

	#
	# Notifications
	#

	command -v notify-send >/dev/null 2>&1 || exit
	[ ! -e "/tmp/bat_notif$bat" ] && touch "/tmp/bat_notif$bat"
	bat_notif="$(cat "/tmp/bat_notif$bat")"


	# Notify when battery is charged to 80%
	if [ $bat_state = 'c' ]; then
		if [ "$bat_notif" != 80 ] && [ $bat_per -ge 80 ]; then
			notify-send -u normal -i battery-charging -t 999999 "$bat at 80%"
			echo 80 > "/tmp/bat_notif$bat"
		fi

	# Notify when battery discharges to 5%, 10%, 15% and 20%
	elif [ $bat_state = 'd' ]; then
		for per in 5 10 15 20; do
			if [ "$bat_notif" = $per ]; then
				break
			elif [ $bat_per -le $per ]; then
				notify-send -u critical -i battery-low -t 999999 "$bat at ${per}%"
				echo $per > "/tmp/bat_notif$bat"
				break
			fi
		done

	# Notify when battery is full
	else
		if [ "$bat_notif" != 100 ]; then
			notify-send -u normal -i battery-full -t 999999 "$bat fully charged!"
			echo 100 > "/tmp/bat_notif$bat"
		fi
	fi
done
echo
