#!/bin/bash

TIME_SCALE=60    # In seconds
TIME_INTERVAL=5 # Multiplier of TIME_SCALE


ACCUMULATOR=0
while true; do
	if ! ((ACCUMULATOR % TIME_INTERVAL)) && [[ `git status --porcelain` ]]; then
		MSG="Autocommit at $(date +'%d.%m.%Y @ %H:%M')"

		git add . && git commit -m "$MSG" && git push

		echo $MSG
	fi

	sleep $TIME_SCALE
	((ACCUMULATOR++))
done
