#!/bin/bash

if [ ! -f /tmp/stopwork ]; then
	touch /tmp/stopwork
else
	$(su - kamen -s /bin/bash -c "while [ -f /tmp/stopwork ]; do if [ $(date '+%H') -gt 9 ] && [ $(date '+%H') -lt 21 ]; then pkill Min; fi; sleep 1; done" &) \
	&& rm /tmp/stopwork
fi
