#!/bin/bash

if [ -z "$@" ] || ([ "$1" != "+" ] && [ "$1" != "-" ]); then
	echo "Expected + or - as argument!"
	exit 1
fi

# Percentage change depending on brightness
info=$(brightnessctl -m i | cut -d , -f 4)
info=${info::-1}

change=5
if [ "$info" -ge 70 ]; then
	change=10
fi

# Brightness change
if [ "$1" == "+" ]; then
	brightnessctl -q set "+${change}%"
elif [ "$info" -gt 5 ]; then # don't allow brightness to decrease to 0%
	brightnessctl -q set "${change}%-"
fi

# Notification for brightness
info=$(brightnessctl -m i)
perbright=$(echo $info | cut -d , -f 4)
notify-send "$(echo $info | cut -d , -f 1)
Brightness: $perbright" -t 1000 -h int:value:$perbright -h string:x-canonical-private-synchronous:anything -h string:x-canonical-private-synchronous:anything
