#!/bin/bash

function update {
	while :; do
		curr_status=$(dunstctl is-paused)
		
		if [ "$curr_status" != "$prev_status" ]; then
			prev_status=$curr_status
			
			if [ "$curr_status" == "true" ]; then
				echo icon:/usr/share/icons/tabler-icon-bell-off.png
				echo tooltip:Notifications OFF
			else
				echo icon:/usr/share/icons/tabler-icon-bell.png
				echo tooltip:Notifications ON
			fi
		fi
		
		sleep 0.2
	done
}

update | \
yad --notification                        \
	--listen                              \
	--command="dunstctl set-paused toggle"
