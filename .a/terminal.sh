#!/bin/bash
terminator --new-tab --working-directory="$(pwd)"

# To set it as default on cinnamon:
# gsettings set org.cinnamon.desktop.default-applications.terminal exec /home/kamen/Commonwealth/terminal.sh
