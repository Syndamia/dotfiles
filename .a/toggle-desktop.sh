#!/bin/bash

if xprop -root _NET_SHOWING_DESKTOP | egrep '= 1' > /dev/null; then
	wmctrl -k off
else
	wmctrl -k on
fi
