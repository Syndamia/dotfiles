#!/bin/bash

if [ -d ".git" ]; then
	sudo mv .git .git.disabled
else
	sudo mv .git.disabled .git
fi
