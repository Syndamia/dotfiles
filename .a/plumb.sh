#/bin/bash


#
# Get selection text and go to window directory
#

selection="$(xclip -o)"

# Copied from https://odysee.com/@Luke:7/plumbing-in-linux-la-plan-9-from-bell:e
pid=$(xprop -id "$(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}')" | grep -m 1 PID | cut -d " " -f 3)
pid1=$(pstree -lpA "$pid" | tail -n 1 | awk -F'---' '{print $NF}' | sed -re 's/[^0-9]//g')
cd "$(readlink /proc/"$pid1"/cwd)"

wname="$(cat /proc/"$pid"/comm)"
if [ $wname == "terminator" ]; then
	xdotool key Control+Alt+Page_Up
	sleep 0.1
	cd "$(cat /tmp/plumb-cwd)"
fi

#
# Create jgmenu menu
#

menu=""
if [ -z "$1" ]; then
	function matches() {
		echo "$selection" | grep "$1" >/dev/null 2>&1
	}

	# Settings, 1 for include in menu, 0 for don't include
	plan9=0
	runInTerminal=1

	# -= Plan 9 functions =-
	[ $plan9 -eq 1 ] && menu+="Send,echo 2\nResize,echo 3\nMove,echo 4\n"

	# -= Run =-
	if [ $runInTerminal -eq 1 ]; then
		[ $wname == "terminator" ] && [ $plan9 -ne 1 ] && menu+="Send,echo 2\n"
		[ $wname == "terminator" ] && menu+="Exec,echo 5\n" \
		                           || menu+="Exec,echo 6\n"
	fi
	#matches "[[:alnum:]]*" && menu+="Run,echo 2\n"
	#matches ".*\.cpp" && menu+="Build and run,echo 2\n"
fi

#
# Execution menu option
#

if [ -z "$1" ]; then
	action=$(echo -ne "$menu" | jgmenu --at-pointer --simple --config-file="$HOME/.config/jgmenu/jgmenusimplerc")
else
	action="1"
fi

case "$action" in
	 1) xdg-open "$selection"
		;;
	 2) xdotool type --delay 0 "$selection"
		;;
	 3) xdotool keydown Alt
		xdotool mousedown 3
		xdotool keyup Alt
		;;
	 4) xdotool keydown Alt
		xdotool mousedown 1
		xdotool keyup Alt
		;;
	 5) terminator --new-tab --command="zsh $selection" &
		disown
		;;
	 6) terminator --command="$selection" &
		disown
		;;
	 *)
		notify-send "Bad action!"
		;;
esac
