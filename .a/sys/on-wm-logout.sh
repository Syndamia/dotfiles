#!/bin/bash

# Gracefully close certain programs
grace=()
gui=(brave firefox)

for app in $grace; do
	timeout 5 pkill -TERM $app
done

for app in $gui; do
	wmctrl -c $app
	while [ -n "$(pidof $app)" ]; do
		sleep 1
	done
done

exit 0
