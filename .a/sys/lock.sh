#!/bin/bash
# Suspend is managed by elogind
# Refer to ~/.b/etc/elogind/logind.conf
#          ~/.b/etc/elogind/system-sleep/run-lock.sh

## Helper functions
res () {
	pkill $@
	$@ 2>&1 >~/nohup.out &
	disown
}

# ++=================================++
# || Actions before and after unlock ||
# ++=================================++

before_locking () {
	# Disables dunst, my notification daemon, so I don't see notification on lock screen
	# https://github.com/dunst-project/dunst/issues/697
	# https://wiki.archlinux.org/title/Dunst#Disable_dunst_temporarily
	dunstctl set-paused true
	sleep 0.1 # Could fix notifications sometimes being shown https://github.com/dunst-project/dunst/issues/697#issuecomment-1045122705

	touch ~/.tmp/locksh-locked
}

before_suspend () {
	# Keep track of time when machine is suspended, as well as how long it has been up for
	uptime >> ~/.a/sys/suspend-history
}

when_unlocked () {
	dunstctl set-paused false # Enables my notification daemon

	# Resets programs, in case resolution changes
	res ~/.fehbg # wallpaper
	# res lead     # hot corners, this is MageJohn's fork: https://github.com/MageJohn/lead

	rm ~/.tmp/locksh-locked
}

# ++==================================++
# || Prevent locking because of audio ||
# ++==================================++

# This makes sure to not lock the screen if audio is playing and there are no arguments given
# It's mainly for preventing automatic screen locking while a video/music is playing
# If you still want to lock, just give it some argument, doesn't matter what
if [ $(grep -r 'RUNNING' /proc/asound | wc -l) -gt 0 ] && [ $# -eq 0 ]; then
	exit 1
fi

# Show an error message if user is trying to suspend, while audio is playing, because that can mess up sound servers
# It tries to send a message via zenity, xmessage or notify-send, depending on which is installed
# but it always sends a text message to stdout. In the end, script execution is stopped.
if [ $(grep -r 'RUNNING' /proc/asound | wc -l) -gt 1 ]; then
	message='Please, stop all playing audio before trying to suspend!'

	if [ -x "$(command -v zenity)" ]; then
		zenity --error --no-wrap --text="$message"
	elif [ -x "$(command -v xmessage)" ]; then
		xmessage -center "$message"
	elif [ -x "$(command -v notify-send)" ]; then
		notify-send -u critical "$message"
	fi
	echo $message

	exit 1
fi

# ++============================++
# || Lock if not locked already ||
# ++============================++

if  [ ! -f ~/.tmp/locksh-locked ]; then
	# The first line of i3lock arguments uses standard arguments, while the rest of the lines are i3lock-color ones
	# If you want to use i3lock, remove those lines (and the \ at the end)
	# But I highly advise you to use i3lock-color, it's amazing: https://github.com/Raymo111/i3lock-color
	(
	before_locking &&                                                                                                                  \
	i3lock -f -c d49408 -i ~/.a/sys/lockscreen.png -n                                                                                  \
		-k --indicator --keylayout 1 --radius 95 --pass-volume-keys                                                                 \
		--date-str='%d.%m.%Y' --verif-text='Verifying…' --wrong-text='Wrong!' --noinput-text='No Input!'                               \
		--insidever-color d47408cc --ringver-color d47408 --insidewrong-color c35b5bcc --ringwrong-color c35b5b                        \
		--layout-color ffeede --time-color ffeede --date-color ffeede --greeter-color ffeede --verif-color ffeede --wrong-color ffeede \
	; when_unlocked
	) &
fi
