#!/bin/bash

device="desktop"

# xhost +si:localuser:root       # fixes unable to open display errors: https://askubuntu.com/a/1130413
~/.fehbg &                     # wallpaper
# lead &                         # hot corners, this is MageJohn's fork: https://github.com/MageJohn/lead
xcape -e 'Super_L=Super_L|P' & # used for the "windows" menu
sleep 1 && /usr/libexec/polkit-gnome-authentication-agent-1 &
rm -rf ~/.tmp/*

dunst &                                                                         # notification daemon
lxpolkit &                                                                      # polkit
xautolock -detectsleep -secure -time 15 -locker ~/.a/sys/lock.sh &      # auto lock screen
tint2 &                                                                         # taskbar
picom &                                                                         # compositor
setxkbmap -layout "us,bg-custom" -option "grp:alt_shift_toggle,lv3:switch,caps:escape" -v & # keyboard switching
sxhkd -c "$HOME/.sxhkdrc" &
gentoo-pipewire-launcher &

if [[ $device == "desktop" ]]; then
	xset m 1 1 # disable mouse acceleration (not a very proper way to do it, I know)
fi

if [[ $device == "laptop" ]]; then
	# while :; do if grep -Fq "closed" /proc/acpi/button/lid/LID0/state; then ~/.a/sys/lock.sh suspend && sleep 3; fi; sleep 0.5; done & # lock screen on lid closing; you'll also need to uncomment the HandleLidSwitch variable and assign to it "lock" (HandleLidSwitch=lock) inside /etc/systemd/logind.conf
	libinput-gestures-setup start & # start gestures
	blueberry &                 # bluetooth app
fi

pnmixer &                      # volume applet
nm-applet &                    # network applet
~/.a/dunst-toggler-applet.sh & # notification toggler
devilspie2 &                   # updates windows icons to the ones form the current theme

flameshot &    # screenshot utility
redshift-gtk & # screen color temperature changer
copyq &        # clipboard manager
barrier &      # share mouse and keyboard between devices
nextcloud &    # cloud/source control

# electron-mail &
# gtk-launch discord &
firefox-bin &
claws-mail &
