*/* pulseaudio

media-sound/pulseaudio -daemon
media-video/pipewire sound-server -daemon
>=app-emulation/libvirt-8.9.0 lxc
x11-misc/tint2 battery
# sys-libs/ncurses -gpm # temporary, https://forums.gentoo.org/viewtopic-t-1062884-start-0.html https://bugs.gentoo.org/602690

# barrier {{{

x11-misc/barrier gui

# }}}

# libratbag {{{

dev-libs/libratbag::gentoo elogind
# }}}


# tlp {{{

### https://github.com/dywisor/tlp-portage repo
app-laptop/tlp bluetooth

# }}}

# gd {{{

media-libs/gd fontconfig jpeg png truetype

# }}}

# vim {{{

app-editors/vim x11 perl lua terminal

# }}}

# tint2 {{{

x11-misc/tint2 tint2conf

# }}}

# redshift {{{

x11-misc/redshift gtk

# }}}

# evince {{{

app-text/evince djvu

# }}}

# dunst {{{

x11-misc/dunst dunstify

# }}}

# picom {{{

x11-misc/picom opengl

# }}}

# pnmixer {{{

media-sound/pnmixer libnotify

# }}}

# texmaker {{{

app-text/poppler      qt5
dev-qt/qtwebengine    widgets
app-text/texlive-core xetex

# }}}

# audacious {{{

media-plugins/audacious-plugins aac ampache bs2b cdda cue ffmpeg flac fluidsynth gme http jack lame libnotify libsamplerate lirc mms modplug mp3 opengl pulseaudio scrobbler sdl sid sndfile soxr speedpitch streamtuner vorbis wavpack

# }}}

# simple-scan {{{

media-gfx/iscan          gimp
media-gfx/sane-backends  usb
media-gfx/sane-frontends gimp gtk
dev-libs/libgusb         vala
x11-misc/colord          vala

# }}}

# moc {{{

media-sound/moc aac alsa ffmpeg flac libsamplerate mad modplug musepack oss sid sndfile speex timidity tremor vorbis wavpack

# }}}

# virt-manager {{{

net-misc/spice-gtk usbredir
net-dns/dnsmasq    script

# }}}

# gajim {{{

net-im/gajim                   jingle networkmanager remote rst upnp

# }}}

# cups {{{

net-print/cups           static-libs usb xinetd zeroconf
gnome-base/libgnomeprint cups
x11-libs/gtk+            cups

# }}}

# qemu {{{

app-emulation/qemu usbredir spice gtk

# }}}

# calibre {{{

dev-python/PyQt5         widgets printsupport gui webchannel network

# }}}

# obs-studio {{{

>=media-video/ffmpeg-4.4.3 nvenc
media-video/obs-studio jack pulseaudio pipewire v4l nvenc

# }}}

# fonts-meta {{{

media-fonts/fonts-meta cjk ms
media-fonts/source-han-sans l10n_ja
media-fonts/corefonts tahoma

# }}}

# opentoonz {{{

>=dev-qt/qtmultimedia-5.15.8 widgets
=dev-qt/qtmultimedia-5.15.7 widgets

# }}}

# libreoffice-bin {{{

app-office/libreoffice-bin zeroconf

# }}}

# apache2 {{{

www-servers/apache apache2_modules_access_compat

# }}}

# inkscape {{{

media-gfx/inkscape                  imagemagick svg2 inkjar jemalloc lcms static-libs

# }}}

# dpkg {{{

app-arch/dpkg static-libs

# }}}

# nextcloud-client {{{

net-misc/nextcloud-client webengine

# }}}

# quassel {{{

dev-db/sqlite -secure-delete static-libs

# }}}

# claws-mail {{{

mail-client/claws-mail webkit
dev-libs/libindicate   gtk

# }}}

# evolution {{{

app-arch/gnome-autoar gtk

# }}}

# apache2 {{{

app-eselect/eselect-php apache2
dev-lang/php apache2

# }}}

# python {{{

*/python tk

# }}}

# ffmpeg {{{

media-video/ffmpeg x265

# }}}

# gnome-disk-utility {{{

sys-apps/gnome-disk-utility fat gnome

# }}}

# feh {{{

media-gfx/feh xinerama

# }}}

# krita {{{

>=dev-python/PyQt5-5.15.7 declarative

# }}}

# gvfs {{{

gnome-base/gvfs mtp # android file transfer

# }}}
