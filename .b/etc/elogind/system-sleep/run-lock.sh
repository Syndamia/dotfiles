#!/bin/bash
# Thanks https://wiki.gentoo.org/wiki/Elogind#Suspend.2FHibernate_Resume.2FThaw_hook_scripts
case $1/$2 in
	pre/*) # Put here any commands expected to be run when suspending or hibernating.
		export DISPLAY=:0
		sudo -H -u kamen bash -c '~/.a/sys/lock.sh && until xdotool search --onlyvisible --classname i3lock; do sleep 0.2; done'
		;;
	post/*) # Put here any commands expected to be run when resuming from suspension or thawing from hibernation.
		;;
esac
